package com.example.nasaapodapplication.data

import android.annotation.SuppressLint
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.nasaapodapplication.Values
import com.example.nasaapodapplication.datamodels.PictureOfDayModel
import com.example.nasaapodapplication.domain.DetailRepository
import com.example.nasaapodapplication.localbatabase.PictureOfDayDao
import com.example.nasaapodapplication.localbatabase.PictureOfDayRoomDataClass
import com.example.nasaapodapplication.retrofit.ApiInterfaceTanslate
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class DetailRepositoryImpl(
    private val database: PictureOfDayDao,
    private val api: ApiInterfaceTanslate
) : DetailRepository {
    private val tag = "DetailRepository"

    override fun translateText(text: String): Single<String> {
        return api.translateText(Values.Translate_API_KEY, text, "en-ru")
            .flatMap { response ->
                Log.d("detail", "response code ${response.code()}")
                when (response.code()) {
                    200 -> {
                        return@flatMap Single.just(response.body()?.text?.get(0))
                    }
                    else -> throw Exception("Not success response code: ${response.code()}")
                }
            }
    }

    @SuppressLint("CheckResult")
    override fun addPictureOfDayInDataBase(): Completable {
        return database.addPictureOfDay(
            PictureOfDayRoomDataClass(
                Values.pictureOfDayWhoNeedAddInDB.copyright,
                Values.pictureOfDayWhoNeedAddInDB.date,
                Values.pictureOfDayWhoNeedAddInDB.explanation,
                Values.pictureOfDayWhoNeedAddInDB.hdurl,
                Values.pictureOfDayWhoNeedAddInDB.media_type,
                Values.pictureOfDayWhoNeedAddInDB.service_version,
                Values.pictureOfDayWhoNeedAddInDB.title,
                Values.pictureOfDayWhoNeedAddInDB.url
            )
        )

    }

    override fun deleteItemInDB(date: String): Completable {
        return database.deleteItemInDB(date)
    }

    private fun writingInDataBase(it: PictureOfDayModel): PictureOfDayModel {
        Log.d(tag, " addInDB wright in db ${it.title}")
        database.addPictureOfDay(
            PictureOfDayRoomDataClass(
                it.copyright,
                it.date,
                it.explanation,
                it.hdurl,
                it.media_type,
                it.service_version,
                it.title,
                it.url
            )
        )
        return it
    }

    @SuppressLint("CheckResult")
    fun addPictureOfDayInDataBase2() {
        Observable.just(Values.pictureOfDayModel)
            .map { writingInDataBase(it) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    Log.d(tag, "wright in db ${it.title}")
                }, {}, {}
            )

    }
}