package com.example.nasaapodapplication.data

import android.annotation.SuppressLint
import android.util.Log
import com.example.nasaapodapplication.domain.LocalDatabaseRepository
import com.example.nasaapodapplication.localbatabase.PictureOfDayDao
import com.example.nasaapodapplication.localbatabase.PictureOfDayRoomDataClass
import io.reactivex.Completable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.BehaviorSubject

class LocalDatabaseRepositoryImpl(
    private val database: PictureOfDayDao
) : LocalDatabaseRepository {
    private val tag = "LocalDatabaseRepository"
    private val dataFromDB: BehaviorSubject<List<PictureOfDayRoomDataClass>> =
        BehaviorSubject.create()
    private val listWithDate: BehaviorSubject<List<String>> = BehaviorSubject.create()

    init {
        // getAllPicturesOfDayFromDB()
        // Values.dateList = this.returnDateList()
    }

    @SuppressLint("CheckResult")
    override fun getAllPicturesOfDayFromDB(): BehaviorSubject<List<PictureOfDayRoomDataClass>> {
        database.getAllPictureOfDay().subscribeBy(onNext = { addDataInList(it) })
        return dataFromDB
    }

    @SuppressLint("CheckResult")
    override fun deleteItemInDB(date: String): Completable {
        return database.deleteItemInDB(date)
    }

    private fun addDataInList(list: List<PictureOfDayRoomDataClass>) {
        Log.d(tag, "data from DB = ${list.size}")
        val array = ArrayList<String>()
        for (item in list) {
            array.add(item.date)
        }
        listWithDate.onNext(array)
        dataFromDB.onNext(list)
    }

    override fun getListWithDate(): BehaviorSubject<List<String>> {
        return listWithDate
    }

}