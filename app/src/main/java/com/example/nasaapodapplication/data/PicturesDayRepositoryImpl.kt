package com.example.nasaapodapplication.data

import android.annotation.SuppressLint
import android.util.Log
import com.example.nasaapodapplication.Values
import com.example.nasaapodapplication.datamodels.PictureOfDayModel
import com.example.nasaapodapplication.domain.PicturesDayRepository
import com.example.nasaapodapplication.localbatabase.PictureOfDayDao
import com.example.nasaapodapplication.localbatabase.PictureOfDayRoomDataClass
import com.example.nasaapodapplication.retrofit.ApiInterface
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.subjects.BehaviorSubject
import java.lang.Exception

import java.util.*
import kotlin.collections.ArrayList


class PicturesDayRepositoryImpl(
    private val database: PictureOfDayDao,
    private val api: ApiInterface
) : PicturesDayRepository {

    @SuppressLint("CheckResult")
    override fun getPictureOfDay(date: String): Single<PictureOfDayModel> {
        return api.getPictureOfDay(date, Values.API_KEY)
            .flatMap { response ->
                Log.d(this.javaClass.name, "response code ${response.code()}")
                when (response.code()) {
                    200 -> {
                        return@flatMap Single.just(response.body())
                    }
                    else -> throw Exception("Not success response code: ${response.code()}")
                }
            }

    }

    @SuppressLint("CheckResult")
    override fun deleteItemInDB(date: String): Completable {
        return database.deleteItemInDB(date)
    }

    override fun addPictureOfDayInDataBase(): Completable {
        return database.addPictureOfDay(
            PictureOfDayRoomDataClass(
                Values.pictureOfDayWhoNeedAddInDB.copyright,
                Values.pictureOfDayWhoNeedAddInDB.date,
                Values.pictureOfDayWhoNeedAddInDB.explanation,
                Values.pictureOfDayWhoNeedAddInDB.hdurl,
                Values.pictureOfDayWhoNeedAddInDB.media_type,
                Values.pictureOfDayWhoNeedAddInDB.service_version,
                Values.pictureOfDayWhoNeedAddInDB.title,
                Values.pictureOfDayWhoNeedAddInDB.url
            )
        )
    }
}