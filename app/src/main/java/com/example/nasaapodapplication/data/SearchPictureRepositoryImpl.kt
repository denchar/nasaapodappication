package com.example.nasaapodapplication.data

import android.annotation.SuppressLint
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.nasaapodapplication.Values
import com.example.nasaapodapplication.datamodels.PictureOfDayModel
import com.example.nasaapodapplication.domain.SearchPictureRepository
import com.example.nasaapodapplication.localbatabase.PictureOfDayDao
import com.example.nasaapodapplication.localbatabase.PictureOfDayRoomDataClass
import com.example.nasaapodapplication.retrofit.ApiClient
import com.example.nasaapodapplication.retrofit.ApiInterface
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import retrofit2.Response
import java.lang.Exception

class SearchPictureRepositoryImpl(
    private var database: PictureOfDayDao,
    private val api: ApiInterface
) : SearchPictureRepository {
    private val compositeDisposable = CompositeDisposable()
    private val tag = this.javaClass.name

    fun search(date: String) {
        compositeDisposable.add(
            api.getPictureOfDay(date, Values.API_KEY)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ Log.d(tag, "${it.code()}") }, { })
        )
    }

    @SuppressLint("CheckResult")
    fun addPictureOfDayInDB() {
        Observable.just(Values.pictureOfDayModel)
            .map { addInDB(it) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    Log.d(tag, "wright in db ${it.title}")
                }, {}, {}
            )
    }

    private fun addInDB(it: PictureOfDayModel): PictureOfDayModel {
        Log.d(tag, " addInDB wright in db ${it.title}")
        database.addPictureOfDay(
            PictureOfDayRoomDataClass(
                it.copyright,
                it.date,
                it.explanation,
                it.hdurl,
                it.media_type,
                it.service_version,
                it.title,
                it.url
            )
        )
        return it
    }

    @SuppressLint("CheckResult")
    override fun searchPictureOfDay(date: String): Single<PictureOfDayModel> {
        return api.getPictureOfDay(date, Values.API_KEY)
            .flatMap { response ->
                Log.d(this.javaClass.name, "response code ${response.code()}")
                when (response.code()) {
                    200 -> {
                        return@flatMap Single.just(response.body())
                    }
                    else -> throw Exception("Not success response code: ${response.code()}")
                }
            }

    }

    @SuppressLint("CheckResult")
    override fun deleteItemInDB(date: String): Completable {
        return database.deleteItemInDB(date)
    }

    override fun addPictureOfDayInDataBase(): Completable {
        return database.addPictureOfDay(
            PictureOfDayRoomDataClass(
                Values.pictureOfDayWhoNeedAddInDB.copyright,
                Values.pictureOfDayWhoNeedAddInDB.date,
                Values.pictureOfDayWhoNeedAddInDB.explanation,
                Values.pictureOfDayWhoNeedAddInDB.hdurl,
                Values.pictureOfDayWhoNeedAddInDB.media_type,
                Values.pictureOfDayWhoNeedAddInDB.service_version,
                Values.pictureOfDayWhoNeedAddInDB.title,
                Values.pictureOfDayWhoNeedAddInDB.url
            )
        )

    }
}