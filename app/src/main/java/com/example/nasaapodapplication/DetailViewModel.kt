package com.example.nasaapodapplication


import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.nasaapodapplication.data.DetailRepositoryImpl
import com.example.nasaapodapplication.domain.DetailInteractor
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import org.koin.core.KoinComponent
import org.koin.core.get

class DetailViewModel : ViewModel(), KoinComponent {
    private val detailInteractor: DetailInteractor = get()
    private val translatedTextResponse = MutableLiveData<String>()
    private val listWithDate = MutableLiveData<List<String>>()
    private val copyrightYandex =
        """
            Переведено сервисом «Яндекс.Переводчик» http://translate.yandex.ru
        """
    private val compositeDisposable = CompositeDisposable()


    init {
        Values.listWithDate
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onNext = { listWithDate.value = it }
            ).addTo(compositeDisposable)
    }

    fun getListWithDate():MutableLiveData<List<String>>{
        return listWithDate
    }

    fun getTranslatedText():MutableLiveData<String>{
        return translatedTextResponse
    }

    fun translateText(text: String) {
        detailInteractor.translateText(text)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onError = { translatedTextResponse.value = (it.message + copyrightYandex) },
                onSuccess = { translatedTextResponse.value = (it + copyrightYandex) }
            ).addTo(compositeDisposable)
    }

    fun addPictureOfDayInDataBase() {
        if (Values.listWithDate.value?.contains(Values.pictureOfDayWhoNeedAddInDB?.date) == false) {
            detailInteractor.addPictureOfDayInDataBase()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(onError = {},
                    onComplete = {
                        Log.d(
                            this.javaClass.name,
                            " ${Values.pictureOfDayWhoNeedAddInDB?.title} was write in database"
                        )
                    })
                .addTo(compositeDisposable)
        }
    }

    fun deleteItemInDB(date: String) {
        detailInteractor.deleteItemInDB(date)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onComplete = {
                Log.d(
                    this.javaClass.name,
                    "$date was delete from DataBase"
                )
            }
            ).addTo(compositeDisposable)
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }
}