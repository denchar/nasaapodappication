package com.example.nasaapodapplication.di

import androidx.room.Room
import com.example.nasaapodapplication.DetailActivity
import com.example.nasaapodapplication.DetailViewModel
import com.example.nasaapodapplication.data.DetailRepositoryImpl
import com.example.nasaapodapplication.data.LocalDatabaseRepositoryImpl
import com.example.nasaapodapplication.data.PicturesDayRepositoryImpl
import com.example.nasaapodapplication.data.SearchPictureRepositoryImpl
import com.example.nasaapodapplication.domain.*
import com.example.nasaapodapplication.localbatabase.PictureOfDayDatabase
import com.example.nasaapodapplication.retrofit.ApiClient
import com.example.nasaapodapplication.retrofit.ApiClientTranslate
import com.example.nasaapodapplication.retrofit.ApiInterface
import com.example.nasaapodapplication.retrofit.ApiInterfaceTanslate
import com.example.nasaapodapplication.ui.pictures_day.PicturesDayAdapter
import com.example.nasaapodapplication.ui.pictures_day.PicturesDayViewModel
import com.example.nasaapodapplication.ui.local_database.LocalDatabaseAdapter
import com.example.nasaapodapplication.ui.local_database.LocalDatabaseViewModel
import com.example.nasaapodapplication.ui.search_picture.SearchPictureAdapter
import com.example.nasaapodapplication.ui.search_picture.SearchPictureViewModel
import com.example.nasaapodapplication.ui.search_picture.SearchPictureViewModel2
import org.koin.android.ext.koin.androidApplication
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

object DependencyModule {
    val picturesOfDayModule = module {
        factory { ApiClient.getApiRetrofitClient()?.create(ApiInterface::class.java) }
        factory { PicturesDayRepositoryImpl(get(),get()) as PicturesDayRepository }
        factory { PicturesDayInteractorImpl() as PicturesDayInteractor }
        viewModel { PicturesDayViewModel() }
        factory { PicturesDayAdapter() }
        single {
            Room.databaseBuilder(
                androidApplication(),
                PictureOfDayDatabase::class.java,
                PictureOfDayDatabase.DB_NAME
            ).build()
        }
        single { get<PictureOfDayDatabase>().getPictureOfDayDao() }
    }
    val detailModule = module {
        factory { ApiClientTranslate.getApiRetrofitClient()?.create(ApiInterfaceTanslate::class.java) }
        factory { DetailRepositoryImpl(get(),get()) as DetailRepository }
        viewModel { DetailViewModel() }
        factory { DetailActivity() }
        factory { DetailInteractorImpl() as DetailInteractor}
    }
    val localDatabaseModule = module {
        factory { LocalDatabaseAdapter() }
        factory { LocalDatabaseRepositoryImpl(get()) as LocalDatabaseRepository }
        factory { LocalDatabaseInteractorImpl() as LocalDatabaseInteractor }
        viewModel { LocalDatabaseViewModel() }
    }
    val searchPictureModule = module{
        factory { SearchPictureRepositoryImpl(get(),get()) as SearchPictureRepository }
        factory { SearchPictureInteractorImpl() as SearchPictureInteractor }
        viewModel { SearchPictureViewModel() }
        viewModel { SearchPictureViewModel2() }
        factory { SearchPictureAdapter() }
    }
}