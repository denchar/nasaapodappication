package com.example.nasaapodapplication.di

import android.app.Application
import com.example.nasaapodapplication.di.DependencyModule.detailModule
import com.example.nasaapodapplication.di.DependencyModule.localDatabaseModule
import com.example.nasaapodapplication.di.DependencyModule.picturesOfDayModule
import com.example.nasaapodapplication.di.DependencyModule.searchPictureModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class NasaApodApp: Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@NasaApodApp)
            modules(listOf(picturesOfDayModule,detailModule,localDatabaseModule,searchPictureModule))
        }
    }

}