package com.example.nasaapodapplication.domain

import com.example.nasaapodapplication.Values
import io.reactivex.Completable
import io.reactivex.Single
import org.koin.core.KoinComponent
import org.koin.core.inject

interface DetailInteractor {
    fun translateText(text: String):Single<String>
    fun addPictureOfDayInDataBase():Completable
    fun deleteItemInDB(date: String):Completable
}

class DetailInteractorImpl : DetailInteractor, KoinComponent {

    private val detailRepository:DetailRepository by inject()

    override fun translateText(text: String):Single<String> {
        return detailRepository.translateText(text)
    }

    override fun addPictureOfDayInDataBase():Completable {
        return detailRepository.addPictureOfDayInDataBase()
    }

    override fun deleteItemInDB(date: String): Completable {
        return detailRepository.deleteItemInDB(date)
    }

}