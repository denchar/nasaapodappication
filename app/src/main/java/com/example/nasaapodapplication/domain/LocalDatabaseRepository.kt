package com.example.nasaapodapplication.domain

import com.example.nasaapodapplication.localbatabase.PictureOfDayRoomDataClass
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.subjects.BehaviorSubject

interface LocalDatabaseRepository {
    fun getAllPicturesOfDayFromDB(): BehaviorSubject<List<PictureOfDayRoomDataClass>>
    fun deleteItemInDB(date: String): Completable
    fun getListWithDate():BehaviorSubject<List<String>>
}