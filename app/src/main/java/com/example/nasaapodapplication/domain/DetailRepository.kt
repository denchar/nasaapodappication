package com.example.nasaapodapplication.domain

import io.reactivex.Completable
import io.reactivex.Single

interface DetailRepository {
    fun translateText(text: String): Single<String>
    fun addPictureOfDayInDataBase(): Completable
    fun deleteItemInDB(date: String): Completable
}