package com.example.nasaapodapplication.domain

import com.example.nasaapodapplication.data.LocalDatabaseRepositoryImpl
import com.example.nasaapodapplication.localbatabase.PictureOfDayRoomDataClass
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.subjects.BehaviorSubject
import org.koin.core.KoinComponent
import org.koin.core.inject

interface LocalDatabaseInteractor {
    fun getAllPicturesOfDayFromDB(): BehaviorSubject<List<PictureOfDayRoomDataClass>>
    fun deleteItemInDB(date: String):Completable
    fun getListWithDate(): BehaviorSubject<List<String>>
}

class LocalDatabaseInteractorImpl : LocalDatabaseInteractor, KoinComponent {

    private val repository: LocalDatabaseRepository by inject()

    override fun getAllPicturesOfDayFromDB(): BehaviorSubject<List<PictureOfDayRoomDataClass>> {
        return repository.getAllPicturesOfDayFromDB()
    }

    override fun deleteItemInDB(date: String):Completable {
        return repository.deleteItemInDB(date)
    }

    override fun getListWithDate(): BehaviorSubject<List<String>> {
        return repository.getListWithDate()
    }

}