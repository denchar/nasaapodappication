package com.example.nasaapodapplication.domain

import com.example.nasaapodapplication.datamodels.PictureOfDayModel
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.subjects.BehaviorSubject

interface PicturesDayRepository {
    fun addPictureOfDayInDataBase(): Completable
    fun deleteItemInDB(date: String): Completable
    fun getPictureOfDay(date: String): Single<PictureOfDayModel>
}