package com.example.nasaapodapplication.domain

import android.util.Log
import com.example.nasaapodapplication.data.PicturesDayRepositoryImpl
import com.example.nasaapodapplication.datamodels.PictureOfDayModel
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.Singles.zip
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import org.koin.core.KoinComponent
import org.koin.core.inject

interface PicturesDayInteractor {
    fun addPictureOfDayInDataBase(): Completable
    fun deleteItemInDB(date: String): Completable
    fun getPicturesOfDay(date: String): Single<PictureOfDayModel>
}

class PicturesDayInteractorImpl : PicturesDayInteractor, KoinComponent {
    private val repository: PicturesDayRepository by inject()

    override fun addPictureOfDayInDataBase(): Completable {
        return repository.addPictureOfDayInDataBase()
    }

    override fun deleteItemInDB(date: String): Completable {
        return repository.deleteItemInDB(date)
    }

    override fun getPicturesOfDay(date: String): Single<PictureOfDayModel> {
        return repository.getPictureOfDay(date)
            .subscribeOn(Schedulers.io())
            .map { it -> return@map it }
    }

}