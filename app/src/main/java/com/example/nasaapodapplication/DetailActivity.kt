package com.example.nasaapodapplication

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import androidx.lifecycle.Observer
import com.example.nasaapodapplication.databinding.ActivityDetailBinding
import com.example.nasaapodapplication.datamodels.PictureOfDayModel
import com.squareup.picasso.Picasso
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import org.koin.android.viewmodel.ext.android.viewModel
import java.io.File
import java.io.FileOutputStream
import java.lang.Exception

class DetailActivity : AppCompatActivity() {

    private val detailViewModel: DetailViewModel by viewModel()
    private var binding: ActivityDetailBinding? = null
    private var imageView: ImageView? = null

    var tag = this.javaClass.name
    private var responseModel: PictureOfDayModel? = null
    var translateText = ""

    companion object {
        fun goToDetailActivity(context: Context) {
            context.startActivity(Intent(context, DetailActivity::class.java))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailBinding.inflate(layoutInflater)
        val view = binding?.root
        setContentView(view)

        imageView = binding?.imageView
        responseModel = Values.pictureOfDayModel
        binding?.textViewTitle?.text = responseModel?.title
        Picasso.get().load(responseModel?.url).placeholder(R.drawable.ic_launcher_background)
            .into(imageView)
        binding?.textContent?.text = responseModel?.explanation
        binding?.playButton?.text = responseModel?.title
        if (responseModel?.media_type == "video") {
            Log.d(this.tag, "log")
            Log.d(this.localClassName.toString(),"${responseModel?.toString()}")
            binding?.imageView?.visibility = View.INVISIBLE
            binding?.playButton?.setOnClickListener {
               /* startActivity(
                    Intent(Intent.ACTION_VIEW,
                        Uri.parse(responseModel?.url)))
                */

                WebViewActivity.goToWebViewActivity(this)
            }
        }
        if (responseModel?.media_type == "image") {
            binding?.playButton?.visibility = View.INVISIBLE
            binding?.sharedButton?.setOnClickListener {
                sharedPicture(
                    this,
                    translateText,
                    responseModel?.url.toString()
                )
            }
        }
        binding?.imageView?.setOnClickListener {
            WebViewActivity.goToWebViewActivity(this)
        }

        detailViewModel.getListWithDate().observe(this, Observer<List<String>> {
            if (it.contains(responseModel?.date)) {
                binding?.detailAddInDb?.visibility = View.INVISIBLE
                binding?.detailDeleteItemInDb?.visibility = View.VISIBLE
            } else {
                binding?.detailAddInDb?.visibility = View.VISIBLE
                binding?.detailDeleteItemInDb?.visibility = View.INVISIBLE
            }
        })


        binding?.detailAddInDb?.setOnClickListener {
            Toast.makeText(this, "${responseModel?.title} add in db", Toast.LENGTH_SHORT).show()

            Values.pictureOfDayWhoNeedAddInDB.date = responseModel?.date.toString()
            Values.pictureOfDayWhoNeedAddInDB.title = responseModel?.title.toString()
            Values.pictureOfDayWhoNeedAddInDB.copyright = responseModel?.copyright.toString()
            Values.pictureOfDayWhoNeedAddInDB.explanation = responseModel?.explanation.toString()
            Values.pictureOfDayWhoNeedAddInDB.hdurl = responseModel?.hdurl.toString()
            Values.pictureOfDayWhoNeedAddInDB.media_type = responseModel?.media_type.toString()
            Values.pictureOfDayWhoNeedAddInDB.service_version =
                responseModel?.service_version.toString()
            Values.pictureOfDayWhoNeedAddInDB.url = responseModel?.url.toString()


            detailViewModel.addPictureOfDayInDataBase()
            // binding!!.detailAddInDb.visibility = View.INVISIBLE
            // binding!!.detailDeleteItemInDb.visibility = View.VISIBLE
        }
        binding?.detailDeleteItemInDb?.setOnClickListener {
            Toast.makeText(this, "${responseModel?.title} delete in db", Toast.LENGTH_SHORT).show()
            responseModel?.date?.let { it1 -> detailViewModel.deleteItemInDB(it1) }
            // binding!!.detailDeleteItemInDb.visibility = View.INVISIBLE
            // binding!!.detailAddInDb.visibility = View.VISIBLE
        }

        responseModel?.let { detailViewModel.translateText(it.explanation) }
        detailViewModel.getTranslatedText().observe(this, Observer<String> {
            translateText = it
            binding?.translateTextContent?.text = it
        })
    }


    @SuppressLint("CheckResult")
    private fun sharedPicture(context: Context, string: String, image: String) {

        shared(context, string, image)
        // Observable.just().subscribeOn(Schedulers.io()).subscribe({ println("Shared")},{},{})

    }

    private fun shared(context: Context, string: String, image: String) {
        Picasso.get().load(image).into(object : com.squareup.picasso.Target {
            override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {
                println("Picasso fail send bitmap")
            }


            override fun onPrepareLoad(placeHolderDrawable: Drawable?) {

            }

            @SuppressLint("QueryPermissionsNeeded")
            override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                val bitmapUri = bitmap?.let { getLocalBitmapUri(it) }
                val intent = Intent(Intent.ACTION_SEND)
                intent.action = Intent.ACTION_SEND_MULTIPLE
                intent.type = "image/text/plain"
                // "text/plain"

                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
                intent.putExtra(Intent.EXTRA_STREAM, bitmapUri)
                intent.putExtra(Intent.EXTRA_TEXT, string)


                if (intent.resolveActivity(context.packageManager) != null) {
                    // startActivity(context, intent, null)
                    startActivity(Intent.createChooser(intent, "Поделиться"))
                }

            }


        }
        )
    }

    fun getLocalBitmapUri(bitmap: Bitmap): Uri {
        val file =
            File(this.getExternalFilesDir(Environment.DIRECTORY_PICTURES), "hamster.png")
        val fileOutputStream = FileOutputStream(file)
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream)
        fileOutputStream.flush()
        fileOutputStream.close()
        file.setReadable(true, true)
        return Uri.fromFile(file)
    }

}
