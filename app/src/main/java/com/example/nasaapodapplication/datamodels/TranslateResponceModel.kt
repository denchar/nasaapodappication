package com.example.nasaapodapplication.datamodels

data class TranslateResponceModel(val code: String, val lang: String, val text: List<String>) {
}