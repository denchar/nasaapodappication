package com.example.nasaapodapplication.datamodels.experimental

import androidx.lifecycle.MutableLiveData
import com.example.nasaapodapplication.datamodels.PictureOfDayModel

data class ViewState(val pictureOfDay :PictureOfDayModel,
                     val errorMessage :String,
                     val listWithDate :List<String>)
