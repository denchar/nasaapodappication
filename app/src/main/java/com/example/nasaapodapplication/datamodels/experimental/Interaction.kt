package com.example.nasaapodapplication.datamodels.experimental

sealed class Interaction {
    data class PictureOfDayChanged(val date: String) : Interaction()
    data class ErrorMessageChanged(val errorMessage :String) : Interaction()
    data class ListWithDateChanged(val listWithDate :List<String>) : Interaction()
    object SearchPictureClick : Interaction()
    object AddPictureOfDayInDataBaseClick : Interaction()
    object DeleteItemInDBClick : Interaction()

}
