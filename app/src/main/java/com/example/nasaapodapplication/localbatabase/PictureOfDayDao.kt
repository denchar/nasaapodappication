package com.example.nasaapodapplication.localbatabase

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single


@Dao
interface PictureOfDayDao {
    @Insert
    fun addPictureOfDay(pictureOfDayRoomDataClass: PictureOfDayRoomDataClass):Completable

    @Query("SELECT * from picture_of_day_table")
    fun getAllPictureOfDay() : Flowable<List<PictureOfDayRoomDataClass>>

    @Query("SELECT * from picture_of_day_table")
    fun getAllFromDB(): Observable<List<PictureOfDayRoomDataClass>>

    @Query("SELECT * from picture_of_day_table WHERE Date=:date")
    fun getPictureByDate(date:String): List<PictureOfDayRoomDataClass>

    @Query("DELETE from picture_of_day_table")
    fun deleteAllPictureOfDay()

    @Query("DELETE from picture_of_day_table WHERE Date=:date")
    fun deleteItemInDB(date: String):Completable
}