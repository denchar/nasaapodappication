package com.example.nasaapodapplication.localbatabase

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "picture_of_day_table")
data class PictureOfDayRoomDataClass(
    @ColumnInfo(name = "copyright") var copyright: String,
    @ColumnInfo(name = "date") var date: String,
    @ColumnInfo(name = "explanation") var explanation: String,
    @ColumnInfo(name = "hdurl") var hdurl: String,
    @ColumnInfo(name = "media_type") var media_type: String,
    @ColumnInfo(name = "service_version") var service_version: String,
    @ColumnInfo(name = "title") var title: String,
    @ColumnInfo(name = "url") var url: String
) {
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0
}