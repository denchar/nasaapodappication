package com.example.nasaapodapplication.localbatabase

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [PictureOfDayRoomDataClass::class], version = PictureOfDayDatabase.VERSION)
abstract class PictureOfDayDatabase : RoomDatabase() {

    abstract fun getPictureOfDayDao(): PictureOfDayDao

    companion object {
        const val DB_NAME = "nasaapoddatabase.db"
        const val VERSION = 1
        @Volatile
        private var INSTANCE: PictureOfDayDatabase? = null

        fun getInstance(context: Context): PictureOfDayDatabase? {
            if (INSTANCE == null) {
                synchronized(PictureOfDayDatabase) {
                    INSTANCE = Room.databaseBuilder(
                        context.applicationContext,
                        PictureOfDayDatabase::class.java, DB_NAME
                    )
                        .fallbackToDestructiveMigration() //убрать
                        .allowMainThreadQueries()
                        .build()
                }
            }

            return INSTANCE
        }
    }
    fun destroyInstance() {
        INSTANCE = null
    }
}