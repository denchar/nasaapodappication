package com.example.nasaapodapplication

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import com.example.nasaapodapplication.databinding.ActivityWebViewBinding

class WebViewActivity : AppCompatActivity() {
    private lateinit var webView: WebView
    private var _binding: ActivityWebViewBinding? = null
    private val binding get() = _binding!!

    companion object {
        fun goToWebViewActivity(context: Context) {
            context.startActivity(Intent(context, WebViewActivity::class.java))
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityWebViewBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)


        webView = binding.webView
        webView.settings.javaScriptEnabled = true
        webView.settings.builtInZoomControls = true
        webView.webViewClient = MyWebViewClient()
        if (savedInstanceState == null) {
                Values.pictureOfDayModel.let {
                   // webView.loadUrl(it.hdurl)
                    webView.loadUrl(it.url)
                }
        }

    }
    // url=https://www.youtube.com/embed/ubBzcSD8G8k?rel=0
    override fun onBackPressed() {
        if (webView.canGoBack()) {
            webView.goBack()
        } else {
            super.onBackPressed()
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        webView.saveState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        webView.restoreState(savedInstanceState)
    }

    private inner class MyWebViewClient : WebViewClient() {
        override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
            view.loadUrl(url)
            return true
        }
    }
}
