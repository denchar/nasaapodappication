package com.example.nasaapodapplication.ui.local_database

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.nasaapodapplication.R
import com.example.nasaapodapplication.databinding.ItemViewPictureFromDbBinding
import com.example.nasaapodapplication.localbatabase.PictureOfDayRoomDataClass
import com.squareup.picasso.Picasso


class LocalDatabaseAdapter : RecyclerView.Adapter<LocalDatabaseAdapter.ViewHolder>() {
    private var data = ArrayList<PictureOfDayRoomDataClass>()
    private var itemClickListener: ItemClick? = null
    private var delButtonListener: DelFromDbClick? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemViewPictureFromDbBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ), itemClickListener, delButtonListener
        )
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(data[position])
    }

    fun setItemClickListener(itemClickListener: ItemClick) {
        this.itemClickListener = itemClickListener
    }

    fun setDelButtonClickListener(delButtonListener: DelFromDbClick) {
        this.delButtonListener = delButtonListener
    }

    fun setData(list: List<PictureOfDayRoomDataClass>) {
        data.clear()
        data.addAll(list)
        notifyDataSetChanged()
    }

    interface ItemClick {
        fun onItemClick(item: PictureOfDayRoomDataClass?)
    }

    interface DelFromDbClick {
        fun onDelFromDbClick(item: PictureOfDayRoomDataClass?)
    }

    class ViewHolder(
        private val binding: ItemViewPictureFromDbBinding,
        private val itemClick: ItemClick?,
        private val delFromDbClick: DelFromDbClick?
    ) : RecyclerView.ViewHolder(binding.root) {
        val date: TextView = binding.textViewDate
        val title: TextView = binding.textViewTitle
        private val imageView: ImageView = binding.imageViewPicture
        private val delButton: ImageButton = binding.delItemButton
        private var responseModel: PictureOfDayRoomDataClass? = null

        init {
            itemView.setOnClickListener {
                itemClick?.onItemClick(responseModel)
            }
            delButton.setOnClickListener {
                delFromDbClick?.onDelFromDbClick(responseModel)
            }
        }

        fun bind(responseModel: PictureOfDayRoomDataClass) {
            this.responseModel = responseModel
            date.text = responseModel.date
            title.text = responseModel.title
            if (responseModel.media_type == "video") {
                Picasso.get()
                    .load(R.drawable.playbtn)
                    .placeholder(R.drawable.ic_baseline_access_time_24)
                    .error(R.drawable.ic_baseline_adb_24)
                    .into(imageView)
            } else if (responseModel.media_type == "image") {
                Picasso.get()
                    .load(responseModel.url)
                    .placeholder(R.drawable.ic_baseline_access_time_24)
                    .error(R.drawable.ic_baseline_adb_24)
                    .into(imageView)
            }

        }


    }
}