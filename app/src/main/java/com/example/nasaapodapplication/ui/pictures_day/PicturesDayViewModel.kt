package com.example.nasaapodapplication.ui.pictures_day

import android.annotation.SuppressLint
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.nasaapodapplication.Values
import com.example.nasaapodapplication.data.PicturesDayRepositoryImpl
import com.example.nasaapodapplication.datamodels.PictureOfDayModel
import com.example.nasaapodapplication.domain.PicturesDayInteractor
import com.example.nasaapodapplication.domain.PicturesDayInteractorImpl
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import org.koin.core.KoinComponent
import org.koin.core.get
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class PicturesDayViewModel : ViewModel(), KoinComponent {

    private val picturesDayInteractor: PicturesDayInteractor = get()
    private val compositeDisposable = CompositeDisposable()
    private var data = MutableLiveData<List<PictureOfDayModel>>()
    private val arrayListOfDayModel = ArrayList<PictureOfDayModel>()
    private val errorMessage = MutableLiveData<String>()
    private val listWithDate = MutableLiveData<List<String>>()

    init {
        sendForcedRequest()

        Values.listWithDate
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onNext = { listWithDate.value = it }
            ).addTo(compositeDisposable)

    }

    override fun onCleared() {
        compositeDisposable.clear()
    }

    fun getData(): MutableLiveData<List<PictureOfDayModel>> {
        return data
    }

    fun getErrorMessage(): MutableLiveData<String> {
        return errorMessage
    }

    fun getListWithDate(): MutableLiveData<List<String>> {
        return listWithDate
    }

    fun addPictureOfDayInDataBase() {
        if (Values.listWithDate.value?.contains(Values.pictureOfDayWhoNeedAddInDB.date) == false) {
            picturesDayInteractor.addPictureOfDayInDataBase()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(onError = {},
                    onComplete = {
                        Log.d(
                            this.javaClass.name,
                            " ${Values.pictureOfDayWhoNeedAddInDB.title} was write in database"
                        )
                    }).addTo(compositeDisposable)
        }
    }

    fun deleteItemInDB(date: String) {
        picturesDayInteractor.deleteItemInDB(date)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onComplete = {
                Log.d(
                    this.javaClass.name,
                    "$date was delete from DataBase"
                )
            }
            ).addTo(compositeDisposable)
    }

    fun sendForcedRequest() {
        val list = getDateList()
        list.forEach {
            picturesDayInteractor
                .getPicturesOfDay(it)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                    onSuccess = { result ->
                        arrayListOfDayModel.add(result)
                        data.value = arrayListOfDayModel
                    },
                    onError = { result ->
                        errorMessage.value = result.message
                    }).addTo(compositeDisposable)
        }

    }

    private fun getDateList(): ArrayList<String> {
        val dateList = ArrayList<String>()
        for (item in 0..9) {
            dateList.add(getDate(item))
        }
        return dateList
    }

    @SuppressLint("SimpleDateFormat")
    private fun getDate(day: Int): String {
        val calendar: Calendar = Calendar.getInstance()
        calendar.add(Calendar.DATE, -day)
        val date = calendar.time
        val dateFormat = SimpleDateFormat("YYYY-MM-dd")
        println(dateFormat.format(date))
        return dateFormat.format(date)
    }
}