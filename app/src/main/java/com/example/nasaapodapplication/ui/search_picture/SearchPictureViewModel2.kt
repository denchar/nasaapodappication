package com.example.nasaapodapplication.ui.search_picture

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.nasaapodapplication.Values
import com.example.nasaapodapplication.datamodels.PictureOfDayModel
import com.example.nasaapodapplication.datamodels.experimental.Interaction
import com.example.nasaapodapplication.datamodels.experimental.ViewState
import com.example.nasaapodapplication.domain.SearchPictureInteractor
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import org.koin.core.KoinComponent
import org.koin.core.get

class SearchPictureViewModel2 : ViewModel(), KoinComponent {
/*
    private val searchPictureInteractor: SearchPictureInteractor = get()
    private val compositeDisposable = CompositeDisposable()

    private var currentViewState = ViewState(
        pictureOfDay = PictureOfDayModel("", "", "", "", "", "", "", ""),
        errorMessage = "",
        listWithDate = listOf("")
    )
    private val _viewState = MutableLiveData<ViewState>()
    val viewState: LiveData<ViewState> = _viewState

    init {
        Values.listWithDate
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                submitViewState(currentViewState.copy(listWithDate = it))
            }).addTo(compositeDisposable)
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

    private fun submitViewState(viewState: ViewState) {
        currentViewState = viewState
        _viewState.postValue(currentViewState)
    }

    fun onInteraction(interaction: Interaction) {
        when (interaction) {
            is Interaction.SearchPictureClick -> searchPicture(interaction.date)
            is Interaction.ErrorMessageChanged -> setErrorValue(interaction.errorMessage)
            is
        }
    }

    private fun searchPicture(date: String) {
        searchPictureInteractor.searchPictureOfDay(date)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onSuccess = { submitViewState(currentViewState.copy(pictureOfDay = it)) },
                onError = { submitViewState(currentViewState.copy(errorMessage = it.toString())) }
            ).addTo(compositeDisposable)
    }

    private fun setErrorValue(value: String?) {
        if (value != null) {
            submitViewState(
                currentViewState.copy(
                    errorMessage = value
                )
            )
        }

    }
    */
}