package com.example.nasaapodapplication.ui.local_database

import android.renderscript.Sampler
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.nasaapodapplication.Values
import com.example.nasaapodapplication.data.LocalDatabaseRepositoryImpl
import com.example.nasaapodapplication.domain.LocalDatabaseInteractor
import com.example.nasaapodapplication.localbatabase.PictureOfDayRoomDataClass
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import org.koin.core.KoinComponent
import org.koin.core.get

class LocalDatabaseViewModel : ViewModel(), KoinComponent {

    private val localDatabaseInteractor: LocalDatabaseInteractor = get()
    private val data: MutableLiveData<List<PictureOfDayRoomDataClass>> = MutableLiveData()
    private val compositeDisposable = CompositeDisposable()

    init {
        localDatabaseInteractor.getAllPicturesOfDayFromDB()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = { data.value = it })
            .addTo(compositeDisposable)

        localDatabaseInteractor.getListWithDate()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = { Values.listWithDate.onNext(it) })
            .addTo(compositeDisposable)
    }

    fun getDataFromDB(): MutableLiveData<List<PictureOfDayRoomDataClass>> {
        return data
    }

    fun deleteItemInDB(date: String) {
        localDatabaseInteractor.deleteItemInDB(date)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onComplete = {
                Log.d(
                    this.javaClass.name,
                    "$date was delete from DataBase"
                )
            }
            ).addTo(compositeDisposable)
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }
}