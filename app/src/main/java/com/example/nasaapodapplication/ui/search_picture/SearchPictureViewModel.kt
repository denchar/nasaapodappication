package com.example.nasaapodapplication.ui.search_picture

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.nasaapodapplication.Values
import com.example.nasaapodapplication.datamodels.PictureOfDayModel
import com.example.nasaapodapplication.domain.SearchPictureInteractor
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import org.koin.core.KoinComponent
import org.koin.core.get

class SearchPictureViewModel : ViewModel(), KoinComponent {

    private val searchPictureInteractor: SearchPictureInteractor = get()
    private val compositeDisposable = CompositeDisposable()

    private val _text = MutableLiveData<String>().apply {
        value = "This is search Fragment"
    }
    val text: LiveData<String> = _text
    private val pictureOfDay = MutableLiveData<PictureOfDayModel>()
    private val errorMessage = MutableLiveData<String>()
    private val listWithDate = MutableLiveData<List<String>>()

    init {
        Values.listWithDate
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                listWithDate.value = it
            }).addTo(compositeDisposable)
    }

    fun getListWithDate():MutableLiveData<List<String>> {
        return listWithDate
    }

    fun getPictureOfDay(): MutableLiveData<PictureOfDayModel> {
        return pictureOfDay
    }

    fun getErrorMessage(): MutableLiveData<String> {
        return errorMessage
    }

    fun searchPicture(date: String) {
        searchPictureInteractor.searchPictureOfDay(date)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onSuccess = { pictureOfDay.value = it },
                onError = { errorMessage.value = it.message }
            ).addTo(compositeDisposable)
    }

    fun addPictureOfDayInDataBase() {
        if (Values.listWithDate.value?.contains(Values.pictureOfDayWhoNeedAddInDB.date) == false) {
            searchPictureInteractor.addPictureOfDayInDataBase()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(onError = {},
                    onComplete = {
                        Log.d(
                            this.javaClass.name,
                            " ${Values.pictureOfDayWhoNeedAddInDB.title} was write in database"
                        )
                    }).addTo(compositeDisposable)
        }
    }

    fun deleteItemInDB(date: String) {
        searchPictureInteractor.deleteItemInDB(date)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onError = {},
                onComplete = {
                    Log.d(
                        this.javaClass.name,
                        " $date was delete from DataBase"
                    )
                }).addTo(compositeDisposable)
    }

    override fun onCleared() {
        compositeDisposable.clear()
    }
}