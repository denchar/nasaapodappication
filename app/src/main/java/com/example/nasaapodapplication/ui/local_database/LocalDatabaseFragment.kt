package com.example.nasaapodapplication.ui.local_database

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.example.nasaapodapplication.DetailActivity
import com.example.nasaapodapplication.Values
import com.example.nasaapodapplication.databinding.FragmentLocalDatabaseBinding
import com.example.nasaapodapplication.datamodels.PictureOfDayModel
import com.example.nasaapodapplication.localbatabase.PictureOfDayRoomDataClass
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel

class LocalDatabaseFragment : Fragment(), LocalDatabaseAdapter.DelFromDbClick,
    LocalDatabaseAdapter.ItemClick {

    private val localDatabaseViewModel: LocalDatabaseViewModel by viewModel()
    private val localDatabaseAdapter: LocalDatabaseAdapter by inject()
    private var _binding: FragmentLocalDatabaseBinding? = null
    private val binding get() = _binding!!
    private var itemTouchHelper: ItemTouchHelper? = null

    private var progressBar: ProgressBar? = null
    private var recyclerView: RecyclerView? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        Log.d(tag, "onCreateView")
        _binding = FragmentLocalDatabaseBinding.inflate(inflater, container, false)
        val view = binding.root


        localDatabaseAdapter.setDelButtonClickListener(this)
        localDatabaseAdapter.setItemClickListener(this)
        recyclerView = binding.recyclerViewPictureOfDayHome
        recyclerView?.adapter = localDatabaseAdapter
        progressBar = binding.progressCircularHome
        startProgress()
        localDatabaseViewModel.getDataFromDB()
            .observe(viewLifecycleOwner, Observer<List<PictureOfDayRoomDataClass>> {
                localDatabaseAdapter.setData(it)
                stopProgress()
                Log.d(tag, "set data in adapter ${it.size}")
                if (it.isEmpty()) {
                    Log.d(tag, "no data in db")
                }
            })

        return view
    }

    override fun onItemClick(item: PictureOfDayRoomDataClass?) {
        if (item != null)
            Values.pictureOfDayModel = PictureOfDayModel(
                item.copyright,
                item.date,
                item.explanation,
                item.hdurl,
                item.media_type,
                item.service_version,
                item.title,
                item.url
            )
        context?.applicationContext?.let { DetailActivity.goToDetailActivity(it) }
    }

    override fun onDelFromDbClick(item: PictureOfDayRoomDataClass?) {
        if (item != null) {
            Toast.makeText(context, "${item.title} delete in db", Toast.LENGTH_SHORT).show()
            localDatabaseViewModel.deleteItemInDB(item.date)
        }
    }

    private fun startProgress() {
        Log.d(tag, "start progress")
        recyclerView?.visibility = View.INVISIBLE
        progressBar?.visibility = View.VISIBLE
    }

    private fun stopProgress() {
        Log.d(tag, "stop progress")
        recyclerView?.visibility = View.VISIBLE
        progressBar?.visibility = View.INVISIBLE
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}