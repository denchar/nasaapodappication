package com.example.nasaapodapplication.ui.search_picture


import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import android.widget.AdapterView.OnItemSelectedListener
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.nasaapodapplication.DetailActivity
import com.example.nasaapodapplication.Values
import com.example.nasaapodapplication.databinding.FragmentSearchPictureBinding
import com.example.nasaapodapplication.datamodels.PictureOfDayModel
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class SearchPictureFragment : Fragment(), SearchPictureAdapter.AddInDbClick,
    SearchPictureAdapter.ItemClick, SearchPictureAdapter.DelInDbClick {

    private var _binding: FragmentSearchPictureBinding? = null
    private val binding get() = _binding!!
    private val searchPictureViewModel: SearchPictureViewModel by viewModel()
    private val teg = "SearchPictureFragment"
    private val adapter: SearchPictureAdapter by inject()
    private var editText: EditText? = null
    private var errorText: TextView? = null

     private var spinnerYear: Spinner? = null
     private var spinnerMonth: Spinner? = null
     private var spinnerDay: Spinner? = null
     private var progressBarSearchPicture: ProgressBar? = null
     private var frameLayout: FrameLayout? = null
     private var swipeContainer: SwipeRefreshLayout? = null

    private val yearList = ArrayList<String>()
    private val dayList = ArrayList<String>()
    private val monthList = listOf(
        "01", "02", "03", "04", "05", "06",
        "07", "08", "09", "10", "11", "12"
    )
    var choseYear = "1995"
    var choseMonth = "06"
    var choseDay = "20"


    override fun onItemClick(item: PictureOfDayModel?) {
        if (item != null) {
            Values.pictureOfDayModel = item
            context?.applicationContext?.let { DetailActivity.goToDetailActivity(it) }
        }
    }

    override fun onAddInDbClick(item: PictureOfDayModel?) {
        if (item != null) {
            Toast.makeText(context, "${item.title} add in db", Toast.LENGTH_SHORT).show()
            Values.pictureOfDayWhoNeedAddInDB = item
            searchPictureViewModel.addPictureOfDayInDataBase()
        }
    }

    override fun onDelInDbClick(item: PictureOfDayModel?) {
        if (item != null) {
            Toast.makeText(context, "${item.title} delete in db", Toast.LENGTH_SHORT).show()
            searchPictureViewModel.deleteItemInDB(item.date)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSearchPictureBinding.inflate(inflater, container, false)
        val view = binding.root

        adapter.setItemClickListener(this)
        adapter.setAddButtonClickListener(this)
        adapter.setDeleteButtonClickListener(this)
        adapter.setDateList(Values.listWithDate.value!!)


        val searchButton: Button = binding.searchImageButton
        val recyclerView: RecyclerView = binding.searchRecyclerView
        recyclerView.adapter = adapter

        errorText = binding.badRequestTextView
        spinnerYear = binding.yearSpinner
        spinnerMonth = binding.monthSpinner
        spinnerDay = binding.daySpinner
        progressBarSearchPicture = binding.progressBarSearchPicture
        frameLayout = binding.frameLayout
        swipeContainer = binding.swipeNotificationContainer

        searchButton.setOnClickListener {
            val convertDate = "$choseYear-$choseMonth-$choseDay"
            binding.frameLayout.visibility = View.GONE
            binding.progressBarSearchPicture.visibility = View.VISIBLE
            binding.badRequestTextView.visibility = View.INVISIBLE
            searchPictureViewModel.searchPicture(convertDate)
        }
        binding.swipeNotificationContainer.setOnRefreshListener {
            Toast.makeText(parentFragment?.context, "refreshing", Toast.LENGTH_SHORT).show()
            Thread.sleep(500)
            binding.swipeNotificationContainer.isRefreshing = false
        }
        binding.swipeNotificationContainer.setColorSchemeResources(
            android.R.color.holo_blue_bright,
            android.R.color.holo_green_light,
            android.R.color.holo_orange_light,
            android.R.color.holo_red_light
        )
        searchPictureViewModel.getPictureOfDay()
            .observe(viewLifecycleOwner, Observer<PictureOfDayModel> {
                Log.d(teg, it.title)
                Log.d(teg, it.url)

                binding.frameLayout.visibility = View.VISIBLE
               binding.progressBarSearchPicture.visibility = View.GONE
                adapter.setData(listOf(it))
                recyclerView.visibility = View.VISIBLE

            })

        searchPictureViewModel.getErrorMessage().observe(viewLifecycleOwner, Observer<String> {
            binding.swipeNotificationContainer.isRefreshing = false
            binding.badRequestTextView.text = it
            recyclerView.visibility = View.INVISIBLE
            binding.badRequestTextView.visibility = View.VISIBLE

        })

        searchPictureViewModel.getListWithDate()
            .observe(viewLifecycleOwner, Observer<List<String>> {
                adapter.setDateList(it)
            })

        addYearList()
        addDayList()
        addDateSpinners()

        return view
    }

    private fun addDateSpinners() {
        val adapterYear: ArrayAdapter<String> =
            ArrayAdapter(
                requireContext().applicationContext,
                android.R.layout.simple_spinner_item,
                yearList
            )
        adapterYear.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerYear?.adapter = adapterYear
        spinnerYear?.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                itemSelected: View?, selectedItemPosition: Int, selectedId: Long
            ) {
                val choose = yearList
                choseYear = choose[selectedItemPosition]
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }

        val adapterMonth: ArrayAdapter<String> =
            ArrayAdapter(
                requireContext().applicationContext,
                android.R.layout.simple_spinner_item,
                monthList
            )
        adapterMonth.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerMonth?.adapter = adapterMonth
        spinnerMonth?.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                itemSelected: View?, selectedItemPosition: Int, selectedId: Long
            ) {
                val choose = monthList
                choseMonth = choose[selectedItemPosition]
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }

        val adapterDay: ArrayAdapter<String> =
            ArrayAdapter(
                requireContext().applicationContext,
                android.R.layout.simple_spinner_item,
                dayList
            )
        adapterDay.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerDay?.adapter = adapterDay
        spinnerDay?.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                itemSelected: View?, selectedItemPosition: Int, selectedId: Long
            ) {
                val choose = dayList
                choseDay = choose[selectedItemPosition]
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
    }

    private fun addDayList() {
        for (item in 1..31) {
            dayList.add(item.toString())
        }
    }

    @SuppressLint("SimpleDateFormat")
    private fun addYearList() {
        var yearStep = 0
        val calendar: Calendar = Calendar.getInstance()
        val date = calendar.time
        val dateFormat = SimpleDateFormat("YYYY")
        var stringYear = dateFormat.format(date)
        while (stringYear != "1995") {
            stringYear = getYears(yearStep)
            yearStep++
            yearList.add(stringYear)
        }
    }

    @SuppressLint("SimpleDateFormat")
    private fun getYears(year: Int): String {

        val calendar: Calendar = Calendar.getInstance()
        calendar.add(Calendar.YEAR, -year)
        val date = calendar.time
        val dateFormat = SimpleDateFormat("YYYY")
        return dateFormat.format(date)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }


}