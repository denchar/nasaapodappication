package com.example.nasaapodapplication.ui.detail

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.example.nasaapodapplication.DetailViewModel
import com.example.nasaapodapplication.R
import com.example.nasaapodapplication.Values
import com.example.nasaapodapplication.datamodels.PictureOfDayModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_detail.*
import org.koin.android.viewmodel.ext.android.viewModel
import java.io.File
import java.io.FileOutputStream
import java.lang.Exception

class DetailFragment: Fragment() {
    private val detailViewModel: DetailViewModel by viewModel()

    var teg = "Detail"
    var responceModel: PictureOfDayModel?=null
    var translateText = ""
    private lateinit var textViewTitle:TextView
    private lateinit var textContent:TextView
    private lateinit var imageView:ImageView
    private lateinit var playButton:Button
    private lateinit var sharedButton:Button

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_pictures_day, container, false)

        responceModel = Values.pictureOfDayModel
        textViewTitle = root.findViewById(R.id.text_view_title)
        imageView = root.findViewById(R.id.image_view)
        textContent = root.findViewById(R.id.text_content)
        playButton = root.findViewById(R.id.play_button)
        sharedButton = root.findViewById(R.id.shared_button)

        textViewTitle.text = responceModel?.title
        Picasso.get().load(responceModel?.url).into(imageView)
        textContent.text = responceModel?.explanation
        playButton.text = responceModel?.title
        if (responceModel?.media_type == "video") {
            imageView.visibility = View.INVISIBLE
            playButton.setOnClickListener {
                startActivity(
                    Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse(responceModel?.url)
                    )
                )
            }
        }
        if (responceModel?.media_type == "image") {
            playButton.visibility = View.INVISIBLE
            sharedButton.setOnClickListener {
                context?.applicationContext?.let { it1 ->
                    responceModel?.url?.let { it2 ->
                        sharedPicture(
                            it1,
                            translateText,
                            it2
                        )
                    }
                }
            }
        }

        responceModel?.explanation?.let { detailViewModel.translateText(it) }
      /*  detailViewModel.getResultText.observe(viewLifecycleOwner, Observer<String> {
            translateText = it
            translate_text_content.text = it
        })
        */
        return root
    }

    private fun sharedPicture(context: Context, string: String, image: String) {

        Picasso.get().load(image).into(object : com.squareup.picasso.Target {
            override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {
                println("Picasso fail send bitmap")
            }


            override fun onPrepareLoad(placeHolderDrawable: Drawable?) {

            }

            override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                var intent = Intent(Intent.ACTION_SEND)
                intent.action = Intent.ACTION_SEND_MULTIPLE
                intent.type = "image/text"
                // "text/plain"
                intent.putExtra(Intent.EXTRA_TEXT, string)


                intent.putExtra(Intent.EXTRA_STREAM, getLocalBitmapUri(bitmap!!))

                if (intent.resolveActivity(context.packageManager) != null) {
                    // startActivity(context, intent, null)
                    startActivity(Intent.createChooser(intent, "Поделиться"))
                }

            }

            fun getLocalBitmapUri(bitmap: Bitmap): Uri {
                var file =
                    File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), "hamster.png")
                var fileOutputStream = FileOutputStream(file)
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream)
                fileOutputStream.flush()
                fileOutputStream.close()
                file.setReadable(true, true)
                return Uri.fromFile(file)
            }

        }
        )
    }
}