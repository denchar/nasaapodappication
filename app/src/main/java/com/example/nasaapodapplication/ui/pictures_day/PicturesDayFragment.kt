package com.example.nasaapodapplication.ui.pictures_day

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.example.nasaapodapplication.DetailActivity
import com.example.nasaapodapplication.Values
import com.example.nasaapodapplication.databinding.FragmentPicturesDayBinding
import com.example.nasaapodapplication.datamodels.PictureOfDayModel
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel


class PicturesDayFragment : Fragment(), PicturesDayAdapter.ItemClick,
    PicturesDayAdapter.AddInDbClick, PicturesDayAdapter.DelInDbClick {

    private val picturesDayViewModel: PicturesDayViewModel by viewModel()
    private val picturesDayAdapter: PicturesDayAdapter by inject()
    private var _binding: FragmentPicturesDayBinding? = null
    private val binding get() = _binding!!

    private var progressBar: ProgressBar? = null
    private var recyclerView: RecyclerView? = null
    private var errorTextView: TextView? = null
    private var newRequestButton: Button? = null

    private lateinit var itemTouchHelper: ItemTouchHelper

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentPicturesDayBinding.inflate(inflater, container, false)
        val view = binding.root

        picturesDayAdapter.setItemClickListener(this)
        picturesDayAdapter.setAddButtonClickListener(this)
        picturesDayAdapter.setDeleteButtonClickListener(this)


        recyclerView = binding.recyclerViewPictureOfDay
        recyclerView?.adapter = picturesDayAdapter
        progressBar = binding.progressCircular
        errorTextView = binding.errorMessageTextView
        newRequestButton = binding.newRequestButton
        errorTextView?.visibility = View.INVISIBLE

        newRequestButton?.setOnClickListener {
            picturesDayViewModel.sendForcedRequest()
            startProgress()
        }
        startProgress()

        picturesDayViewModel.getListWithDate().observe(viewLifecycleOwner, Observer {
            picturesDayAdapter.setDateList(it)
        })

        picturesDayViewModel.getData()
            .observe(viewLifecycleOwner, Observer<List<PictureOfDayModel>> {
                picturesDayAdapter.setData(it)
                stopProgress()
                errorTextView?.visibility = View.INVISIBLE
                newRequestButton?.visibility = View.INVISIBLE
            })
        picturesDayViewModel.getErrorMessage().observe(viewLifecycleOwner, Observer<String> {
            errorTextView?.text = it
            errorTextView?.visibility = View.VISIBLE
            newRequestButton?.visibility = View.VISIBLE
            stopProgress()
            Toast.makeText(context, it, Toast.LENGTH_SHORT).show()
        })

        itemTouchHelper = ItemTouchHelper(object : ItemTouchHelper.SimpleCallback(
            0,
            ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT
        ) {
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val position = viewHolder.adapterPosition
                val name = picturesDayAdapter.getItemViewPosition(position).title
                if (ItemTouchHelper.LEFT == direction) {
                    Log.d(tag, "$name left")
                    onAddInDbClick(picturesDayAdapter.getItemViewPosition(position))
                    picturesDayAdapter.notifyItemRemoved(position)
                    picturesDayAdapter.notifyDataSetChanged()
                }
                if (ItemTouchHelper.RIGHT == direction) {
                    Log.d(tag, "$name right")
                }

            }
        })
        itemTouchHelper.attachToRecyclerView(recyclerView)
        return view
    }

    override fun onItemClick(item: PictureOfDayModel?) {
        if (item != null) {
            Values.pictureOfDayModel = item
        }
        context?.applicationContext?.let { DetailActivity.goToDetailActivity(it) }
    }

    override fun onAddInDbClick(item: PictureOfDayModel?) {
        if (item != null) {
            Toast.makeText(context, "${item.title} add in db", Toast.LENGTH_SHORT).show()
            Values.pictureOfDayWhoNeedAddInDB = item
            picturesDayViewModel.addPictureOfDayInDataBase()
        }
    }

    override fun onDelInDbClick(item: PictureOfDayModel?) {
        if (item != null) {
            Toast.makeText(context, "${item.title} delete in db", Toast.LENGTH_SHORT).show()
            Values.pictureOfDayWhoNeedAddInDB = item
            picturesDayViewModel.deleteItemInDB(item.date)
        }
    }

    private fun startProgress() {
        recyclerView?.visibility = View.INVISIBLE
        progressBar?.visibility = View.VISIBLE
        errorTextView?.visibility = View.INVISIBLE
        newRequestButton?.visibility = View.INVISIBLE
    }

    private fun stopProgress() {
        recyclerView?.visibility = View.VISIBLE
        progressBar?.visibility = View.INVISIBLE
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}